all:
	msgfmt de.po -o de.mo
	echo ==== fail case
	cp debmake-doc.xml debmake-doc-fail.xml
	-./itstool -i ./docbook.its -m de.mo -o ./ debmake-doc-fail.xml
	echo ==== succ case
	cp debmake-doc.xml debmake-doc-succ.xml
	./itstool.work -i ./docbook.its -m de.mo -o ./ debmake-doc-succ.xml

clean:
	$(RM) de.mo debmake-doc-fail.xml debmake-doc-succ.xml
